
from rest_framework import serializers
from listings.models import Listing

class ListingsSerializer(serializers.ModelSerializer):

    class Meta:
        model = Listing
        fields = ['listing_type', 'title', 'country', 'price','check_in','check_out']
        related_fields = ['booking_info']

    price = serializers.IntegerField(source='booking_info.price')
    check_in = serializers.DateTimeField(source='booking_info.check_in')
    check_out = serializers.DateTimeField(source='booking_info.check_out')
