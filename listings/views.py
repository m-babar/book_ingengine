
from rest_framework import viewsets
from listings.serializers import ListingsSerializer
from listings.models import Listing
from rest_framework.response import Response


class ListingsViewSet(viewsets.ViewSet):
    """
    API endpoint that to get non booked listing.
    """

    def list(self, request):
        queryset = Listing.objects.all().order_by('booking_info__price')
        if request.query_params.get('max_price'):
            queryset = queryset.filter(booking_info__price__lt=int(request.query_params.get('max_price')))
        if request.query_params.get('check_in') and request.query_params.get('check_out'):
            queryset = queryset.exclude(
                booking_info__check_in__lte=request.query_params.get('check_out'),
                booking_info__check_out__gte=request.query_params.get('check_in'),
                booking_info__listing__listing_type='Apartment'
            )
            queryset = queryset.exclude(
                booking_info__check_in__lte=request.query_params.get('check_out'),
                booking_info__check_out__gte=request.query_params.get('check_in'),
                booking_info__listing__listing_type='Hotel',
                booking_info__hotel_room_type__isnull=True
            )
        serializer = ListingsSerializer(list(queryset), many=True)
        return Response(serializer.data)