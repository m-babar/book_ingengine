
from django.test import TestCase
from rest_framework.test import APITestCase
from rest_framework.test import APIRequestFactory
from listings import views


class AccountTests(APITestCase):

    def test_root_api_url(self):
        """
        To test root api.
        """
        factory = APIRequestFactory()
        view = views.ListingsViewSet.as_view({'get': 'list'})
        request = factory.get("")
        response = view(request)
        self.assertEqual(response.status_code, 200)

    def test_api_filter_response(self):
        """
        To test filter api.
        """
        factory = APIRequestFactory()
        view = views.ListingsViewSet.as_view({'get': 'list'})
        request = factory.get("?max_price=100&check_in=2021-12-09&check_out=2021-12-12")
        response = view(request)
        self.assertEqual(response.status_code, 200)

    def test_api_filter_response(self):
        """
        To test filter api.
        """
        factory = APIRequestFactory()
        view = views.ListingsViewSet.as_view({'get': 'list'})
        request = factory.get("?max_price=90")
        response = view(request)
        self.assertEqual(response.status_code, 200)

    def test_api_filter_response(self):
        """
        To test filter api.
        """
        factory = APIRequestFactory()
        view = views.ListingsViewSet.as_view({'get': 'list'})
        request = factory.get("?max_price=60&check_in=2021-12-09&check_out=2021-12-12")
        response = view(request)
        self.assertEqual(response.status_code, 200)


